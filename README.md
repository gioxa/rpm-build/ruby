# ruby RPM build

Builds a ruby install rpm, with version  according to the tag or defaults to 2.5.1

- With spec-file form [feedforce/ruby-rpm](https://github.com/feedforce/ruby-rpm).
- source for rpm build from [ruby-lang](https://www.ruby-lang.org/en/)
- availleble at [https://repo.deployctl.com/](https://repo.deployctl.com/)

## install

Only for centos7, RPM based,

1. Add the repository to the system:

```bash
curl https://repo.deployctl.com/repo.rpm.sh | sudo bash
```

2. Install

```bash
sudo yum install ruby-2.5.1
```

optional lock version to prevent update

```bash
sudo yum versionlock  ruby-2.5.1
```

Read more on about installing a specific version: [yum-install-specific-package-version](https://www.zulius.com/how-to/yum-install-specific-package-version/)